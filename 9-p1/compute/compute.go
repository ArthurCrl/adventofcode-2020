package compute

import (
	"sort"
	_ "fmt"
	"../types"
)

func ComputeAll(atoms []types.Atom, threshold int) int {
	cpt:= -1
	nbs := []int{}
	for _, a := range atoms {
		nbs = append(nbs,a.Nb)
	}
	for j:= threshold; j<len(nbs); j++ {
		if !IsSumOfPrevious(j, nbs, threshold) {
			cpt = nbs[j]
			break
		}
	}
	return cpt
}

func IsSumOfPrevious(indexFrom int, nbs []int, threshold int) bool {
	// fmt.Printf("nbs:%v\n",nbs)
	slc := make([]int, len(nbs))
	copy(slc,  nbs)
	// fmt.Printf("nbs:%v\n",nbs)
	slc = slc[indexFrom-threshold : indexFrom]
	total := nbs[indexFrom]
	// fmt.Printf("slc:%v\n",slc)
	sort.Ints(slc)
	cleanedSlc := []int{}
	// fmt.Printf("if:%v, trs: %v\n", indexFrom, threshold)
	// fmt.Printf("slc:%v\n",slc)
	// fmt.Printf("t:%v\n",total)
	// fmt.Printf("nbs:%v\n",nbs)
	for _, nb := range slc {
		if nb < total {
			cleanedSlc = append(cleanedSlc, nb)
		}
	}
  for i:= 0 ; i< len(cleanedSlc); i++ {
		for j:= i; j< len(cleanedSlc); j++ {
			if cleanedSlc[i] + cleanedSlc[j] == total {
				// fmt.Printf("i:%v v:%v \n",i, cleanedSlc[i])
				// fmt.Printf("j:%v v:%v \n",j, cleanedSlc[j])
				return true
			}
		}
	}
	return false
}
