package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o types.Atom
}

func TestParseLine(t *testing.T) {
	var tis []tio = []tio{{
		i: "light red bags contain 1 bright white bag, 2 muted yellow bags.",
		o: types.Atom{
			Name: "light red",
			Children: map[string]int{
				"bright white": 1,
				"muted yellow": 2,
			},
		},
	},{
		i: "bright white bags contain 1 shiny gold bag.",
		o: types.Atom{
			Name: "bright white",
			Children: map[string]int {
				"shiny gold": 1,
			},
		},
	},{
		i: "faded blue bags contain no other bags.",
		o: types.Atom{
			Name: "faded blue",
			Children: map[string]int {},
		},
	},
	}
	for _, ti := range tis {
		var r types.Atom = ParseLine(ti.i)
		if !r.Equals(ti.o) {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

