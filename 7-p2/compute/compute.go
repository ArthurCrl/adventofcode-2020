package compute

import (
	// "fmt"

	"../types"
)

func ComputeAll(atoms []types.Atom) int {
	cpt := 0
	mapsOfAtoms := BuildMapOfAtoms(atoms)
	a := *mapsOfAtoms["shiny gold"]
	m := BuildMolecule(a, mapsOfAtoms)
	cpt = ComputeChildren(m, &a, 0) - 1 

	return cpt
}

func ComputeChildren(m types.Molecule, rootAtom *types.Atom, numberOfBags int) int {
	for _, c := range m.Children {
		if c.Current.Name != (*rootAtom).Name {
			cNb := ComputeChildren(*c, rootAtom, 0)
			numberOfBags = numberOfBags + c.Quantity * cNb
		}
	}
	return numberOfBags +1
}

func BuildMolecule(a types.Atom, mapOfAtoms map[string]*types.Atom) types.Molecule {
	c := []*types.Molecule{}
	for n, q := range a.Children {
		o := BuildMolecule(*mapOfAtoms[n], mapOfAtoms)
		o.Quantity = q
		c = append(c, &o)
	}
	m := types.Molecule{
		Current:  &a,
		Children: c,
	}
	return m
}

func BuildMapOfAtoms(atoms []types.Atom) map[string]*types.Atom {
	m := make(map[string](*types.Atom))
	for i := 0; i < len(atoms); i++ {
		a := atoms[i]
		m[a.Name] = &a
	}
	return m
}
