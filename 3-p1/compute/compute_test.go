package compute

import (
	"strconv"
	"testing"
	"../types"
)

type tio struct {
	i []types.Atom
	o int
}

func TestComputeAll(t *testing.T) {
	var tis []tio = []tio{{
		i: []types.Atom{{
			Row: "..##.......",
		},{
			Row: "#...#...#..",
		},{
			Row: ".#....#..#.",
		},{
			Row: "..#.#...#.#",
		},{
			Row: ".#...##..#.",
		},{
			Row: "..#.##.....",
		},{
			Row: ".#.#.#....#",
		},{
			Row: ".#........#",
		},{
			Row: "#.##...#...",
		},{
			Row: "#...##....#",
		},{
			Row: ".#..#...#.#",
		},
		},
		o: 7,
	}}
	for _, ti := range tis {
		var r type.Result = ComputeAll(ti.i)
		if r != ti.o {
			t.Log("With input " + ti.i.String()+ " result should be " + strconv.FormatBool(ti.o) + ", but got " + strconv.FormatBool(r))
			t.Fail()
		}
	}
}











