package compute

import (
	"../types"
)

func ComputeAll(atoms []types.Atom) types.Result {
	cpt := 0
	index_row := 0
  index_col :=0
	for index_row < len(atoms) {
		atom := atoms[index_row]
		if ComputeAtom(atom,index_col) {
			cpt++
		}
		index_row++
		index_col=index_col+3
	}
	return types.Result{
		Res:  cpt,
		Err: nil,
	}
}

func ComputeAtom(atom types.Atom, index int) bool {
	return atom.Get(index) == types.Tree
}
