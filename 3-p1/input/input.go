package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"../types"
)

func ParseAll() []types.Atom {
	absPath, err := filepath.Abs("./input/input.txt")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Atom = []types.Atom{}
	for scanner.Scan() {
		rtrn = append(rtrn, parseLine(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func parseLine(s string) types.Atom {
	return types.Atom{
		Row:  s,
	}
}
