package compute

import (
	"fmt"
	"sort"
	"../types"
)

func ComputeAll(atoms []types.Atom) int {

	nbs := []int{0}
	for _, a := range atoms {
		nbs = append(nbs,a.Nb)
	}
	_, max := FindMinMax(nbs)
	maxJolts := max + 3
	nbs = append(nbs, maxJolts)
	sort.Ints(nbs)
	ones := 0
	threes := 0
	graph := []*types.Molecule{}
	diffs := []int{}
	for i, nb := range nbs {
		next := -1
		diff := 0
		if  i+1 >= len(nbs) {
			next = -1
			diff = 3
		} else {
			next = nbs[i+1]
			diff = next-nb
		}
		fmt.Printf("%v %v %v\n", nb, next, diff)
		if diff == 1 {
			ones = ones +1
		}
		if diff == 3 {
			threes = threes +1
		}
		diffs = append(diffs, diff)
		mol := types.Molecule{
			Id: nb,
			Delta: diff,
			PossibleNext: []int{},
		}

		graph = append(graph, &mol)
	}

	for i, mol := range graph {
		nextDelta := 0
		j:= 1
		for nextDelta < 3 && i+j < len(diffs) {
			nextDelta = nextDelta+diffs[i+j]
			mol.PossibleNext = append(mol.PossibleNext, i+j)
			j = j+1
		}
	  fmt.Printf("%v,", *mol)
	}


	subGraph := []*types.Molecule{}
	subGraphs := [][]*types.Molecule{}
	for j:= 0; j< len(graph); j++ {
		fmt.Printf("%v\n", j)
		subGraph = append(subGraph, graph[j])
		if diffs[j] == 3 {
			fmt.Printf("EOG %v\n", j)
			subGraphs = append(subGraphs, subGraph)
			subGraph = []*types.Molecule{}
		} 
	}
	fmt.Printf("%v\n", subGraphs)
	found := 1
	for _, g := range subGraphs {

	fmt.Printf("SUBG: %v,%v,%v\n", g, (*g[0]).Id, (*g[len(g)-1]).Id )
	  found = found * TryPaths(*g[0],(*g[0]).Id, (*g[len(g)-1]).Id, graph,0)
	}



	

	return found
}

func TryPaths(mol types.Molecule, totalJolt int, max int, graph []*types.Molecule, realDelta int) int {
	found := 0
	totalJolt = totalJolt + realDelta

	fmt.Printf("%v,%v,%v,%v\n", totalJolt, max, mol.Delta, mol)
	if totalJolt == (max) {

	 fmt.Printf("FOUND\n")
		return 1
	}
	if totalJolt>max {
		return found
	}
	
	for i:=0; i< len(mol.PossibleNext);i++ {
		nextMolIndex := mol.PossibleNext[i]
		nextMol := *graph[nextMolIndex]
		rDelta := nextMol.Id - mol.Id
		if rDelta <= 3 {
			found = found + TryPaths(nextMol, totalJolt, max, graph, rDelta)
		}
	}
	if len(mol.PossibleNext) == 0 {
	}
	return found
}


func FindMinMax(nbs []int) (int, int) {
	min := -1
	max := 0

	// fmt.Printf("nbs:%v\n",nbs)
	for _, v := range nbs {
		// fmt.Printf("min:%v max:%v \n",min, max)
		if v < min || min == -1 {
			min = v
		}
		if v > max {
			max = v
		}
	}
	return min, max
}
