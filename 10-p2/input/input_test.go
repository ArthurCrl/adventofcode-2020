package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o types.Atom
}

func TestParseLine(t *testing.T) {
	var tis []tio = []tio{{
		i: "25",
		o: types.Atom{
			Nb : 25,
		},
	},{
		i: "432",
		o: types.Atom{
			Nb : 432,
		},
	},
	}
	for _, ti := range tis {
		var r types.Atom = ParseLine(ti.i)
		if !r.Equals(ti.o) {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

