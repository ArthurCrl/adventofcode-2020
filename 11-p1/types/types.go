package types
import (
	"fmt"
)


type Space string

const(
		Floor Space = "."
		Empty Space = "L"
		Occ Space = "#"
)

type Atom struct {
	AllSpace [][]Space
	Iteration int
}

func (a Atom) Equals(b Atom) bool {
	if len(a.AllSpace) != len(b.AllSpace) {
		return false
	}
	for i := 0; i< len(a.AllSpace); i++ {
		if len(a.AllSpace[i]) != len(b.AllSpace[i]) {
		return false
	}
		for j := 0; j < len(a.AllSpace[i]); j++ {
			if a.AllSpace[i][j] != b.AllSpace[i][j] {
				return false
			}
		}
	}
	return true
}

func (a Atom) String() string {
	s := ""
	s = s + fmt.Sprintf("It.: %v\n", a.Iteration)
	s = s + fmt.Sprintf("Map:\n")
	for i := 0; i< len(a.AllSpace); i++ {
		for j := 0; j < len(a.AllSpace[i]); j++ {
			s = s + fmt.Sprintf("%v", a.AllSpace[i][j])
		}
		s = s + fmt.Sprintf("\n")
	}
	return s
}

type Molecule struct {
}


func (m Molecule) Equals(n Molecule) bool {
	return fmt.Sprintf("%v", m) == fmt.Sprintf("%v", n)
}

func (m Molecule) FromAtoms(atoms *[]Atom) Molecule {
	return m
}
