package compute

import (

	"../types"
)

func ComputeAll(atom types.Atom) int {
	currentAtom := types.Atom{
		Iteration: -1,
		AllSpace:  [][]types.Space{},
	}
	var nextAtom types.Atom = atom
	// fmt.Printf("E:%v\n", currentAtom.Equals(nextAtom))
	for !currentAtom.Equals(nextAtom) {
		currentAtom = nextAtom
		// fmt.Printf("%v\n", currentAtom)
		nextAtom = ComputeOne(currentAtom)
	}
	// fmt.Printf("E:%v\n", currentAtom.Equals(nextAtom))
	// fmt.Printf("%v\n", currentAtom)
	// fmt.Printf("%v\n", nextAtom)
	return ComputeAllSeats(currentAtom.AllSpace)
}

func ComputeOne(atom types.Atom) types.Atom {
	nextAtom := types.Atom{
		Iteration: atom.Iteration + 1,
		AllSpace:  [][]types.Space{},
	}
	for i := 0; i < len(atom.AllSpace); i++ {
		l := []types.Space{}
		nextAtom.AllSpace = append(nextAtom.AllSpace, l)
		for j := 0; j < len(atom.AllSpace[i]); j++ {
			s := atom.AllSpace[i][j]
			if s == types.Floor {
				nextAtom.AllSpace[i] = append(nextAtom.AllSpace[i], s)
				continue
			}
			c := ComputeOccupiedSeats(atom.AllSpace, i, j)
			if s == types.Empty {
				if c == 0 {
					nextAtom.AllSpace[i] = append(nextAtom.AllSpace[i], types.Occ)
					continue
				} else {
					nextAtom.AllSpace[i] = append(nextAtom.AllSpace[i], s)
					continue
				}
			}
			if s == types.Occ {
				if c >= 4 {
					nextAtom.AllSpace[i] = append(nextAtom.AllSpace[i], types.Empty)
					continue
				} else {
					nextAtom.AllSpace[i] = append(nextAtom.AllSpace[i], s)
					continue
				}
			}
		}
	}
	return nextAtom
}

func ComputeOccupiedSeats(allSpace [][]types.Space, a int, b int) int {
	nb := 0
	for i := a - 1; i <= a+1; i++ {
		for j := b - 1; j <= b+1; j++ {
			if i >= 0 && j >= 0 && i < len(allSpace) && j < len(allSpace[i]) {
				// fmt.Printf("%v,%v, %v\n", i,j,allSpace[i][j] )
				// fmt.Printf("%v,%v\n", len(allSpace), len(allSpace[i]) )
				if allSpace[i][j] == types.Occ {
					 if i == a && j == b{
						 continue 
					 } else {

						//  fmt.Printf("%v\n", "OK" )
					nb = nb + 1
					 }
				}
			}
		}
	}
	// fmt.Printf("NB:%v\n", nb )
	return nb
}

func ComputeAllSeats(allSpace [][]types.Space) int {
	nb := 0
	for i := 0; i < len(allSpace); i++ {
		for j := 0; j < len(allSpace[i]); j++ {
			if allSpace[i][j] == types.Occ {
				nb = nb + 1
			}
		}
	}
	return nb
}
