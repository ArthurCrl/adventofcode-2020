package compute

import (
	"fmt"
	"../types"
	"math"
)

func getMaps() (map[int]types.Move, map[types.Move]int) {
	mapOfAngleToDirection := map[int]types.Move{
		 90: types.North,
		 -270:types.North,
		 0: types.East,
		 360: types.East,
		 -90: types.South,
		 270: types.South,
		 180: types.West,
		 -180: types.West,
	}
	mapOfDirectionToAngle := map[types.Move]int{
		types.North:90,
		types.East: 0,
		types.South: -90,
		types.West: 180,
	}
	return mapOfAngleToDirection, mapOfDirectionToAngle
}
func ComputeAll(atoms []types.Atom) int {
	mol := types.Molecule{
		Longitude: 0,
		Latitude: 0,
		Direction: types.Waypoint{
			Longitude: 1,
			Latitude: 10,
		},
	}
	fmt.Printf("%v\n", mol)
	for _, a := range atoms {
		if a.Action == types.Forward {
			mol.Latitude = mol.Latitude+ mol.Direction.Latitude * a.Nb
			mol.Longitude = mol.Longitude+mol.Direction.Longitude * a.Nb
		} else {
			if a.Action == types.Left {
				ComputeNewDirection(a.Nb,&mol)
			} else if a.Action == types.Right {
				ComputeNewDirection(-1*a.Nb,&mol)
			} else {
				ComputeMove(a.Action, a.Nb, &mol)
			}
		}
		fmt.Printf("%v%v: %v\n", a.Action, a.Nb, mol) 
	}
	return Abs(mol.Longitude)+Abs(mol.Latitude)
}

func ComputeNewDirection( r int, mol *types.Molecule) {
	rads :=  float64(r) * math.Pi/ 180
	newLat :=  int(math.Round(float64((*mol).Direction.Latitude) * math.Cos(rads) - float64((*mol).Direction.Longitude * int(math.Sin(rads)))))
	newLong := int(math.Round(float64((*mol).Direction.Latitude) * math.Sin(rads) + float64((*mol).Direction.Longitude * int(math.Cos(rads)))))
	(*mol).Direction.Longitude = newLong
	(*mol).Direction.Latitude = newLat
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func ComputeMove(m types.Move, nb int,  mol  *types.Molecule) {
	if m == types.North {
		mol.Direction.Longitude = mol.Direction.Longitude + nb
	}
	if m == types.South {
		mol.Direction.Longitude = mol.Direction.Longitude - nb
	}
	if m == types.East {
		mol.Direction.Latitude = mol.Direction.Latitude + nb
	}
	if m == types.West {
		mol.Direction.Latitude = mol.Direction.Latitude - nb
	}
}

