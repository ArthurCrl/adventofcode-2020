package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"../types"
)

func ParseAll() []types.Atom {
	absPath, err := filepath.Abs("./input/input.txt")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Atom = []types.Atom{}
	for scanner.Scan() {
		rtrn = append(rtrn, parseLine(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func GetAllSlops()  []types.Slop {
	return []types.Slop{{
		Right: 1,
		Down: 1,
	}, {
		Right: 3,
		Down: 1,
	}, {
		Right: 5,
		Down: 1,
	}, {
		Right: 7,
		Down: 1,
	}, {
		Right: 1,
		Down: 2,
	}}
}

func parseLine(s string) types.Atom {
	return types.Atom{
		Row:  s,
	}
}
