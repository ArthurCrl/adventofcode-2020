package main

import (
	"./input"
	"./compute"
	"fmt"
)
func main() {
	var atoms = input.ParseAll()
	var slops = input.GetAllSlops()
	var res = compute.ComputeAll(atoms, slops)
	fmt.Println(res)
}
