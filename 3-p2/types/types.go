package types

import (
	"fmt"
)
const Tree = '#'
const Floor = '.'

type Atom struct {
	Row  string
}

type Result struct {
	Res int
	Err error
}

type Slop struct {
	Down int
	Right int
}

func (a Atom) String() string {
	return fmt.Sprintf("{line: %v}", a.Row)
}

func AtomsToString(as []Atom) string {
	rtr := ""
	for _,a := range as {
		rtr = rtr+ fmt.Sprintf("\n%v", a.Row)
	}
	return rtr
	
}

func (a Atom) Get(index int) (byte) {
	actualIndex := index % len(a.Row)
	return a.Row[actualIndex]
}
func (r Result) String() string {
	return fmt.Sprintf("{res: %v, err: %v}", r.Res, r.Err)
}
