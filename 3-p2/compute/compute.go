package compute

import (
	"../types"
)

func ComputeAll(atoms []types.Atom, slops []types.Slop) types.Result {
	prd := 1
	for _, slop := range slops {
		cpt := 0
		index_row := 0
		index_col :=0
		for index_row < len(atoms) {
			atom := atoms[index_row]
			if ComputeAtom(atom,index_col) {
				cpt++
			}
			index_row = index_row + slop.Down
			index_col=index_col+ slop.Right
		}
		prd = prd * cpt
	}
	
	return types.Result{
		Res:  prd,
		Err: nil,
	}
}

func ComputeAtom(atom types.Atom, index int) bool {
	return atom.Get(index) == types.Tree
}
