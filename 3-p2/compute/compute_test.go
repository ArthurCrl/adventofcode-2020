package compute

import (
	"testing"
  "../input"
	"../types"
)

type tio struct {
	i []types.Atom
	o types.Result
}

func TestComputeAll(t *testing.T) {
	var tis []tio = []tio{{
		i: []types.Atom{{
			Row: "..##.......",
		}, {
			Row: "#...#...#..",
		}, {
			Row: ".#....#..#.",
		}, {
			Row: "..#.#...#.#",
		}, {
			Row: ".#...##..#.",
		}, {
			Row: "..#.##.....",
		}, {
			Row: ".#.#.#....#",
		}, {
			Row: ".#........#",
		}, {
			Row: "#.##...#...",
		}, {
			Row: "#...##....#",
		}, {
			Row: ".#..#...#.#",
		},
		},
		o: types.Result {
			Res: 336,
			Err : nil,
		},
	},
	}
	for _, ti := range tis {
		var r = ComputeAll(ti.i, input.GetAllSlops())
		if r != ti.o {
			t.Log("With input " + types.AtomsToString(ti.i) + " result should be " + ti.o.String() + ", but got " + r.String())
			t.Fail()
		}
	}
}
