package compute

import (
	"fmt"
	"sort"
	"strconv"
)

/**
Multiply
*/
func Multiply(inputs []string) int {
	var nbs []int = epureInput(inputs)
	var i, j, k int
	for i = 0; i < len(nbs); i++ {
		var curent = nbs[i]
		for j = i + 1; j < len(nbs); j++ {

			var next = nbs[j]
			for k = j + 1; k < len(nbs); k++ {

				var next_next = nbs[k]
				fmt.Printf("%v - %v - %v - %v \n", curent, next, next_next, curent+next+next_next)

				if curent+next+next_next == 2020 {
					return curent * next * next_next
				}
			}
		}
	}

	return -1
}

func epureInput(inputs []string) []int {
	var numbers []int = []int{}
	for _, v := range inputs {
		var number, _ = strconv.Atoi(v)
		if number < 2021 {
			numbers = append(numbers, number)
		}
	}
	sort.Ints((numbers))
	return numbers
}
