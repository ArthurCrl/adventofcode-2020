package compute

import (
	"strconv"
	"testing"
)

type Tio struct {
	i []string
	o int
}

func TestMultiply(t *testing.T) {
	var tis []Tio = []Tio{Tio{
		i: []string{"1", "2000", "19"},
		o: 38000,
	}, Tio{
		i: []string{"1500","130",  "500", "320", "20"},
		o: 15000000,
	}}
	for _, ti := range tis {

		var r int = Multiply(ti.i)
		if r != ti.o {
			t.Log("error should be " + strconv.Itoa(ti.o) + ", but got " + strconv.Itoa(r))
			t.Fail()
		}
	}
}
