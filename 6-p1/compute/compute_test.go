package compute

import (
	"fmt"
	"testing"
"../input"
	"../types"
)

type tio struct {
	i types.Atom
	o int
}

func TestComputeSeatId(t *testing.T) {
	var tis []tio = []tio{{
		i: input.ParseLine("FBFBBFFRLR"),
		o: 357,
	},{
		i: input.ParseLine("BFFFBBFRRR"),
		o: 567,
	},{
		i: input.ParseLine("FFFBBBFRRR"),
		o: 119,
	},{
		i: input.ParseLine("BBFFBBFRLL"),
		o: 820,
	}}
	for _, ti := range tis {
		var r types.Atom = ComputeSeatId(ti.i)
		if r.SeatId != ti.o {
			t.Log("With input " + fmt.Sprintf("%v", ti.i)+ " result should be " + fmt.Sprintf("%v", ti.o)+ ", but got " + fmt.Sprintf("%v", r.SeatId))
			t.Fail()
		}
	}
}
