package types

import (
	"fmt"
	"testing"
)

type tio struct {
	i Molecule
	o int
}

func TestComputeNbAnswers(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			Atoms: []Atom{{
				Answers: "abc",
			},
			},
		},
		o: 3,
	}, {
		i: Molecule{
			Atoms: []Atom{{
				Answers: "a",
			}, {
				Answers: "b",
			}, {
				Answers: "c",
			},
			},
		},
		o: 3,
	}, {
		i: Molecule{
			Atoms: []Atom{{
				Answers: "ab",
			}, {
				Answers: "ac",
			},
			},
		},
		o: 3,
	}, {
		i: Molecule{
			Atoms: []Atom{{
				Answers: "a",
			}, {
				Answers: "a",
			}, {
				Answers: "a",
			}, {
				Answers: "a",
			},
			},
		},
		o: 1,
	}, {
		i: Molecule{
			Atoms: []Atom{{
				Answers: "b",
			},
			},
		},
		o: 1,
	}}
	for _, ti := range tis {
		var r Molecule = ti.i.ComputeAnswers()
		if r.NbAnswers != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r.NbAnswers))
			t.Fail()
		}
	}
}
