package types
import (
	"fmt"
)
type Atom struct {
	Answers string
}

func (a Atom) Equals(b Atom) bool {
	return fmt.Sprintf("%v", a) == fmt.Sprintf("%v", b)
}

type Molecule struct {
	Atoms []Atom
	AnswersByChar map[rune]bool 
	NbAnswers int
}


func (m Molecule) Equals(n Molecule) bool {
	return fmt.Sprintf("%v", m) == fmt.Sprintf("%v", n)
}

func (m Molecule) FromAtoms(atoms *[]Atom) Molecule {
	m.Atoms = *atoms
	return m
}

func (m Molecule) ComputeAnswers() Molecule {
	m.AnswersByChar = make(map[rune]bool)
	for _, a := range m.Atoms {
		for _, c := range a.Answers {
			m.AnswersByChar[c] = true
		}
	}
	m.NbAnswers = len(m.AnswersByChar)
	return m
} 
