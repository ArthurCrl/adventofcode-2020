package compute

import (
	"fmt"
	"sort"
	"../types"
)

func ComputeAll(atoms []types.Atom) int {

	nbs := []int{}
	for _, a := range atoms {
		nbs = append(nbs,a.Nb)
	}
	_, max := FindMinMax(nbs)
	maxJolts := max + 3
	sort.Ints(nbs)
	ones := 0
	threes := 0
	if nbs[0] == 1 {
		ones = ones +1
	}
	if nbs[0] == 3 {
		threes = threes +1
	} 
	for i, nb := range nbs {
		next := -1
		if  i+1 >= len(nbs) {
			next = maxJolts
		} else {
			next = nbs[i+1]
		}
		diff := next-nb
		fmt.Printf("%v %v %v\n", nb, next, diff)
		if diff == 1 {
			ones = ones +1
		}
		if diff == 3 {
			threes = threes +1
		} 
	}
	return ones*threes
}


func FindMinMax(nbs []int) (int, int) {
	min := -1
	max := 0

	// fmt.Printf("nbs:%v\n",nbs)
	for _, v := range nbs {
		// fmt.Printf("min:%v max:%v \n",min, max)
		if v < min || min == -1 {
			min = v
		}
		if v > max {
			max = v
		}
	}
	return min, max
}
