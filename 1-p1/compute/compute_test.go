package compute

import (
	"strconv"
	"testing"
)

type Tio struct {
	i []string
	o int
}

func TestMultiply(t *testing.T) {
	var tis []Tio = []Tio{Tio{
		i: []string{"1", "2019"},
		o: 2019,
	}, Tio{
		i: []string{"1500","130",  "520", "320"},
		o: 780000,
	}}
	for _, ti := range tis {

		var r int = Multiply(ti.i)
		if r != ti.o {
			t.Log("error should be " + strconv.Itoa(ti.o) + ", but got " + strconv.Itoa(r))
			t.Fail()
		}
	}
}
