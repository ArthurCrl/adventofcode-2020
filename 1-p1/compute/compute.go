package compute

import (
	"fmt"
	"sort"
	"strconv"
)

/**
Multiply
*/
func Multiply(inputs []string) int {
	var nbs []int = epureInput(inputs)
	var i, j int
	for i = 0; i < len(nbs); i++ {
		var curent  = nbs[i]
		for j = i+1; j < len(nbs); j++ {
			var next = nbs[j]
			fmt.Printf("%v - %v - %v\n", curent, next, curent+next)

			if curent+next == 2020 {
				return curent * next
			}
		}
	}

	return -1
}

func epureInput(inputs []string) []int {
	var numbers []int = []int{}
	for _, v := range inputs {
		var number, _ = strconv.Atoi(v)
		if number < 2021 {
			numbers = append(numbers, number)
		}
	}
	sort.Ints((numbers))
	return numbers
}
