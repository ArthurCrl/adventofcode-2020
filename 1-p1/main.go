package main

import (
	"./input"
	"./compute"
	"fmt"
)
func main() {
	var res = compute.Multiply(input.Data)
	fmt.Printf("Result: %v", res)
}
