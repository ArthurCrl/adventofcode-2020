package compute

import (
	"regexp"

	"../types"
)

func ComputeAll(atoms []types.Atom) types.Result {
  cpt := 0
	for _,a := range atoms {
		if ComputeAtom(a) {
			cpt++
		}
	}
	return types.Result{
		Res:  cpt,
		Err: nil,
	}
}

func ComputeAtom(atom types.Atom) bool {
	var regExp = regexp.MustCompile(string(atom.Char))
	var matches = regExp.FindAllString(atom.Pwd, -1)
	var nbMatches = len(matches)
	return nbMatches >= atom.Min && nbMatches <= atom.Max
}
