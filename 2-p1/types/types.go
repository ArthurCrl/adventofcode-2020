package types

import (
	"fmt"
)

type Atom struct {
	Min  int
	Max  int
	Char byte
	Pwd  string
}

type Result struct {
	Res int
	Err error
}

func (a Atom) String() string {
	return fmt.Sprintf("{min: %v, max: %v, char: %v, pwd:%v}", a.Min, a.Max, a.Char, a.Pwd)
}

func (r Result) String() string {
	return fmt.Sprintf("{res: %v, err: %v}", r.Res, r.Err)
}
