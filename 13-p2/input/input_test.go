package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o  []types.Atom
}

func TestParseAll(t *testing.T) {
	var tis []tio = []tio{{
		i: "./test.txt",
		o: []types.Atom{
			types.Atom {
				Id: 7,
				Time: 0,
			},
			types.Atom {
				Id: 13,
				Time: 1,
			},
			types.Atom {
				Id: 59,
				Time: 4,
			},
			types.Atom {
				Id: 31,
				Time: 6,
			},
			types.Atom {
				Id: 19,
				Time: 7,
			},
	}},
}
	for _, ti := range tis {
		var r []types.Atom = ParseAll(ti.i)
		if fmt.Sprintf("%v", r) != fmt.Sprintf("%v", ti.o) {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

