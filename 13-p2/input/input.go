package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"../types"
	"regexp"
	"strconv"
)

func ParseAll(path string) []types.Atom {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Atom = []types.Atom{}
	index := 0
	for scanner.Scan() {
		atom, err := ParseLine(scanner.Text(), index)

		index++
		if err == nil {
		  rtrn = append(rtrn, atom)
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}


func ParseLine(s string, index int) (types.Atom, error) {
	regExp := regexp.MustCompile(`^(x|\d+)$`)
	parsed := regExp.FindStringSubmatch(s)
	var atom types.Atom
	var err error
	var i int
	if parsed != nil {
		nb := 0
		i, err = strconv.Atoi(parsed[1])
		if err == nil {
			nb = i
			atom  = types.Atom{
				Id: nb,
				Time: index,
			}

		}
	}
	
	return atom, err
}
