package compute

import (
	//  "fmt"
	"../types"
)

func ComputeAll(atoms []types.Atom) int {
	var index int = 0
	var step int = atoms[0].Id
	for i := 1; i < len(atoms); i ++ {
		for ((index + atoms[i].Time) % atoms[i].Id != 0) {
			index = index + step
		}
		step = step * atoms[i].Id 
	}
	return index
}