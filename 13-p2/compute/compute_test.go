package compute

import (
	"fmt"
	"testing"
"../input"
	"../types"
)

type tio struct {
	i []types.Atom
	o int
}

func TestComputeAll(t *testing.T) {
	var tis []tio = []tio{{
		i: input.ParseAll("./../input/test2.txt"),
		o: 3417,
	},
	{
		i: input.ParseAll("./../input/test3.txt"),
		o: 754018,
	},
	{
		i: input.ParseAll("./../input/test4.txt"),
		o: 779210,
	},
	{
		i: input.ParseAll("./../input/test5.txt"),
		o: 1261476,
	},
	{
		i: input.ParseAll("./../input/test6.txt"),
		o: 1202161486,
	},
}
	for _, ti := range tis {
		var r int = ComputeAll(ti.i)
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}
