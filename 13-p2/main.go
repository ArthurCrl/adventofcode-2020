package main

import (
	"./input"
	"./compute"
	"fmt"
)
func main() {

	var atoms = input.ParseAll("./input/input.txt")
	var res = compute.ComputeAll(atoms)
	fmt.Println(res)
}

