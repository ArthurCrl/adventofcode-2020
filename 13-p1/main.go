package main

import (
	// "./input"
	// "./compute"
	"fmt"
)
func main() {
	start := 1007268
	mods := []int{17,41,937,13,23,29,397,37,19}
	for _,mod := range mods {
		rest := Rest(start, mod)
		fmt.Printf("m:%v -> %v\n", mod, rest- start)
	}
}


func Rest( start int, mod int) int {
	return mod-start%mod+start
}
