package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"../types"
	"strconv"
)

func ParseAll(path string) []types.Atom {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Atom = []types.Atom{}
	for scanner.Scan() {
		atom := ParseLine(scanner.Text())
		rtrn = append(rtrn, atom)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func ParseLine(s string) types.Atom {
	regExpName := regexp.MustCompile(`(.*) bags contain .*`)
	regExpChildren := regexp.MustCompile(`((((\d+) ([^,.]*) bag(s)?)|(no other bags))([,.]))+`)
	name := regExpName.FindAllStringSubmatch(s, -1)[0][1]
	childrenRegexp := regExpChildren.FindAllStringSubmatch(s, -1)
	// fmt.Printf("%v \n", childrenRegexp)
	atom := types.Atom{
		Name: name,
		Children: make(map[string]int),
	}
	for _, token := range childrenRegexp {
		nb, err :=  strconv.Atoi(token[4])
		// fmt.Printf("%v %v \n", nb, err)
		if err == nil {
			atom.Children[token[5]] = nb
		}
	}
	return atom
}
