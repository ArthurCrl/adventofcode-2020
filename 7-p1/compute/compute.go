package compute

import (
	"../types"
	// "fmt"
)

func ComputeAll(atoms []types.Atom) int {
	cpt := 0
	mapsOfAtoms := BuildMapOfAtoms(atoms)
  // fmt.Printf("Map of atoms: %v\n", mapsOfAtoms)
	for _, a := range atoms {
		// fmt.Printf("Computing %v\n", a)
		m := BuildMolecule(a, mapsOfAtoms)
		mapOfChildren := make(map[string]bool)
		ComputeChildren(m, &mapOfChildren)

		// fmt.Printf("Map built: %v\n", mapOfChildren)
		if mapOfChildren["shiny gold"] {
			cpt++
		}
	}
	return cpt
}

func ComputeChildren(m types.Molecule, mapOfChildren *map[string]bool) {

	// fmt.Printf("Computing children for: %v, %v\n", (*m.Current).Name, m.Children)
	for _, c := range m.Children {
		// fmt.Printf("Child: %v\n", (*c.Current).Name)
		(*mapOfChildren)[(*c.Current).Name] = true
		ComputeChildren(*c, mapOfChildren)
	}
}

func BuildMolecule(a types.Atom, mapOfAtoms map[string] *types.Atom) types.Molecule {
	c := []*types.Molecule{}
	for n, q := range a.Children {
		o := BuildMolecule(*mapOfAtoms[n], mapOfAtoms)
		o.Quantity = q
		c = append(c, &o)
	}
	m := types.Molecule{
		Current : &a,
		Children: c,
	}
	return m
}

func BuildMapOfAtoms(atoms []types.Atom) map[string]*types.Atom {
	m := make(map[string](*types.Atom))
	for i:= 0; i< len(atoms); i++ {
		a:= atoms[i]
		m[a.Name] = &a
	}
	return m
}