package types
import (
	"fmt"
)
type HalfPath string

const(
		UpperHalf HalfPath = "U"
		LowerHalf HalfPath = "L"
)

type Atom struct {
	RowPath  []HalfPath
	ColPath []HalfPath
	Row int
	Col int
	SeatId int
}

func (a Atom) Equals(b Atom) bool {
	return fmt.Sprintf("%v", a) == fmt.Sprintf("%v", b)
}

type Molecule struct {
}


func (m Molecule) FromAtoms(atoms []Atom) Molecule {
	
	return m
}

