package types
import (
	"fmt"
)

type OperationKind string

const(
		Accumulate OperationKind = "acc"
		Jump OperationKind = "jmp"
		Nothing OperationKind = "nop"
)

type Atom struct {
	Kind OperationKind
	Nb int
}

func (a Atom) Equals(b Atom) bool {
	return fmt.Sprintf("%v", a) == fmt.Sprintf("%v", b)
}

type Molecule struct {
}


func (m Molecule) Equals(n Molecule) bool {
	return fmt.Sprintf("%v", m) == fmt.Sprintf("%v", n)
}

func (m Molecule) FromAtoms(atoms *[]Atom) Molecule {
	return m
}
