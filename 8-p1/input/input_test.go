package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o types.Atom
}

func TestParseLine(t *testing.T) {
	var tis []tio = []tio{{
		i: "nop +0",
		o: types.Atom{
			Kind: types.Nothing,
			Nb : 0,
		},
	},{
		i: "acc +1",
		o: types.Atom{
			Kind: types.Accumulate,
			Nb : 1,
		},
	},{
		i: "jmp +46",
		o: types.Atom{
			Kind: types.Jump,
			Nb : 46,
		},
	},{
		i: "jmp -100",
		o: types.Atom{
			Kind: types.Jump,
			Nb : -100,
		},
	},{
		i: "acc -208",
		o: types.Atom{
			Kind: types.Accumulate,
			Nb : -208,
		},
	},
	}
	for _, ti := range tis {
		var r types.Atom = ParseLine(ti.i)
		if !r.Equals(ti.o) {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

