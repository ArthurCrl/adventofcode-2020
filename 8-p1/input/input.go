package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"../types"
	"strconv"
)

func ParseAll(path string) []types.Atom {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Atom = []types.Atom{}
	for scanner.Scan() {
		atom := ParseLine(scanner.Text())
		rtrn = append(rtrn, atom)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func ParseLine(s string) types.Atom {
	regExp := regexp.MustCompile(`^(nop|acc|jmp) (\+|-)(\d+)$`)
	parsed := regExp.FindStringSubmatch(s)
		
	if parsed != nil {
		nb := 0
		i, err := strconv.Atoi(parsed[3])
		if err == nil {
			if parsed[2] == "-" {
				nb = -1 * i
			} else {
				nb = i
			}
		}
		return types.Atom{
				Kind: types.OperationKind(parsed[1]),
				Nb: nb,
			}
	}
	
	return types.Atom{
		Kind: types.Nothing,
		Nb: 0,
	}
}
