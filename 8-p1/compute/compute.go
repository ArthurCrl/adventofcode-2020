package compute

import (
	// "fmt"

	"../types"
)

func ComputeAll(atoms []types.Atom) int {
	cpt := 0

	mapOfVisitedIndexes := map[int]bool{}
	for i:=0; i<len(atoms);i++ {
		if 	mapOfVisitedIndexes[i] {
			break
		}
		mapOfVisitedIndexes[i]= true
		atom := atoms[i]
		if atom.Kind == types.Accumulate {
			cpt = cpt+ atom.Nb
		}
		if atom.Kind == types.Jump {
			i = i+atom.Nb-1
		}
	}

	return cpt
}
