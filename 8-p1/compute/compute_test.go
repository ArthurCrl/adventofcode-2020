package compute

import (
	"fmt"
	"testing"
"../input"
	"../types"
)

type tio struct {
	i []types.Atom
	o int
}

func TestComputeAll(t *testing.T) {
	var tis []tio = []tio{{
		i: input.ParseAll("./../input/test.txt"),
		o: 5,
	}}
	for _, ti := range tis {
		var r int = ComputeAll(ti.i)
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}
