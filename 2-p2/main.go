package main

import (
	"./input"
	"./compute"
	"fmt"
)
func main() {
	var atoms = input.ParseAll()
	var res = compute.ComputeAll(atoms)
	fmt.Println(res)
}
