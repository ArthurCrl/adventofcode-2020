package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"

	"../types"
)

func ParseAll() []types.Atom {
	absPath, err := filepath.Abs("./input/input.txt")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Atom = []types.Atom{}
	for scanner.Scan() {
		rtrn = append(rtrn, parseLine(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func parseLine(s string) types.Atom {
	var regExp = regexp.MustCompile(`^(\d+)-(\d+)\s(\w):\s(.*)$`)
	var parsed = regExp.FindStringSubmatch(s)
	min, err := strconv.Atoi(parsed[1])
	if err != nil {
		log.Fatal(err)
	}
	max, err := strconv.Atoi(parsed[2])
	if err != nil {
		log.Fatal(err)
	}
	var char byte
	if len(parsed[3]) > 0 {
		char = parsed[3][0]
	}
	return types.Atom{
		Min:  min,
		Max:  max,
		Char: char,
		Pwd:  parsed[4],
	}
}
