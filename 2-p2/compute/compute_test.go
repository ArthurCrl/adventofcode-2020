package compute

import (
	"strconv"
	"testing"
	"../types"
)

type tio struct {
	i types.Atom
	o bool
}

func TestComputeAtom(t *testing.T) {
	var tis []tio = []tio{{
		i: types.Atom{
			Min: 1,
			Max: 3,
			Char: 'a',
			Pwd: "abcde",
		},
		o: true,
	}, {
		i:types.Atom{
			Min: 1,
			Max: 3,
			Char: 'b',
			Pwd: "cdefg",
		},
		o: false,
	},{
		i:types.Atom{
			Min: 2,
			Max: 9,
			Char: 'c',
			Pwd: "ccccccccc",
		},
		o: false,
	}}
	for _, ti := range tis {
		var r bool = ComputeAtom(ti.i)
		if r != ti.o {
			t.Log("With input " + ti.i.String()+ " result should be " + strconv.FormatBool(ti.o) + ", but got " + strconv.FormatBool(r))
			t.Fail()
		}
	}
}
