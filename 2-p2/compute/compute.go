package compute

import (

	"../types"
)

func ComputeAll(atoms []types.Atom) types.Result {
  cpt := 0
	for _,a := range atoms {
		if ComputeAtom(a) {
			cpt++
		}
	}
	return types.Result{
		Res:  cpt,
		Err: nil,
	}
}

func ComputeAtom(atom types.Atom) bool {
	if len(atom.Pwd) < atom.Max  {
		return false
	}

	var charAtMin = atom.Pwd[atom.Min -1]
	var charAtMax = atom.Pwd[atom.Max -1]
	if charAtMin == atom.Char && charAtMax == atom.Char {
		return false
	}
	if charAtMin == atom.Char || charAtMax == atom.Char {
		return true
	}
	return false
}
