package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o []types.Molecule
}

func TestParseAll(t *testing.T) {
	var tis []tio = []tio{{
		i: "./test.txt",
		o: []types.Molecule{
			{BirthYear:"1937", IssueYear:"2017", ExpirationYear:"2020", Height:"183cm", HairColor:"#fffffd", EyeColor:"gry", PassportID:"860033327", CountryID:"147"},
			{BirthYear:"1929", IssueYear:"2013", ExpirationYear:"2023", HairColor:"#cfa07d", EyeColor:"amb", PassportID:"028048884", CountryID:"350"},
			{BirthYear:"1931", IssueYear:"2013", ExpirationYear:"2024", Height:"179cm", HairColor:"#ae17e1", EyeColor:"brn", PassportID:"760753108" },
			{IssueYear:"2011", ExpirationYear:"2025", Height:"59in", HairColor:"#cfa07d", EyeColor:"brn", PassportID:"166559648"},
		},
	}}
	for _, ti := range tis {
		var rs []types.Molecule = ParseAll(ti.i)
		t.Log("With input " + ti.i + " received " + fmt.Sprintf("%+v", rs))
		for i, r := range rs {
			var e types.Molecule
			if i < len(ti.o) {
				e = ti.o[i]
			}
			oStr := fmt.Sprintf("%+v", e)
			oR := fmt.Sprintf("%+v", r)
			if r != e {
				t.Log("With input " + ti.i + " result should be " + oStr + ", but got " + oR)
				t.Fail()
			}
		}
	}
}
