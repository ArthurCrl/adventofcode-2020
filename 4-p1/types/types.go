package types

type Atom struct {
	Type  string
	Value string
}

type Molecule struct {
	BirthYear      string
	IssueYear      string
	ExpirationYear string
	Height         string
	HairColor      string
	EyeColor       string
	PassportID     string
	CountryID      string
}

type Result struct {
	Res int
	Err error
}

func (m Molecule) FromAtoms(atoms []Atom) Molecule {
	for _, a := range atoms {
		switch a.Type {
		case "byr":
			m.BirthYear = a.Value
		case "iyr":
			m.IssueYear = a.Value
		case "eyr":
			m.ExpirationYear = a.Value
		case "hgt":
			m.Height = a.Value
		case "hcl":
			m.HairColor = a.Value
		case "ecl":
			m.EyeColor = a.Value
		case "pid":
			m.PassportID = a.Value
		case "cid":
			m.CountryID = a.Value
		}
	}
	return m
}

func (m Molecule) IsValid() bool {
	return m.BirthYear != "" &&
		m.IssueYear != "" &&
		m.ExpirationYear != "" &&
		m.Height != "" &&
		m.HairColor != "" &&
		m.EyeColor != "" &&
		m.PassportID != ""
}

