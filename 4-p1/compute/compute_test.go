package compute

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i []types.Molecule
	o int
}

func TestComputeAtom(t *testing.T) {
	var tis []tio = []tio{{
		i: []types.Molecule{
			{BirthYear:"1937", IssueYear:"2017", ExpirationYear:"2020", Height:"183cm", HairColor:"#fffffd", EyeColor:"gry", PassportID:"860033327", CountryID:"147"},
			{BirthYear:"1929", IssueYear:"2013", ExpirationYear:"2023", HairColor:"#cfa07d", EyeColor:"amb", PassportID:"028048884", CountryID:"350"},
			{BirthYear:"1931", IssueYear:"2013", ExpirationYear:"2024", Height:"179cm", HairColor:"#ae17e1", EyeColor:"brn", PassportID:"760753108" },
			{IssueYear:"2011", ExpirationYear:"2025", Height:"59in", HairColor:"#cfa07d", EyeColor:"brn", PassportID:"166559648"},
		},
		o: 2,
	}}
	for _, ti := range tis {
		var r int = ComputeAll(ti.i)
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%v", ti.i)+ " result should be " + fmt.Sprintf("%v", ti.o)+ ", but got " + fmt.Sprintf("%v", r))
			t.Fail()
		}
	}
}
