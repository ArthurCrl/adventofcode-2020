package compute

import (

	"../types"
)

func ComputeAll(mols []types.Molecule) int {
  cpt := 0
	for _,m := range mols {
		if ComputeMolecule(m) {
			cpt++
		}
	}
	return cpt
}

func ComputeMolecule(mol types.Molecule) bool {
	return mol.IsValid()
}
