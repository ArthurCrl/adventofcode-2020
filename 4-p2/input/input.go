package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"

	"../types"
)

func ParseAll(path string) []types.Molecule {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Molecule = []types.Molecule{}
	var curAtoms []types.Atom = []types.Atom{}
	for scanner.Scan() {
		atoms := parseLine(scanner.Text())
		if len(atoms) > 0 {
			curAtoms = append(curAtoms, atoms...)
		} else {
			rtrn = append(rtrn, types.Molecule{}.FromAtoms(curAtoms))
			curAtoms = []types.Atom{}
		}
	}
	rtrn = append(rtrn, types.Molecule{}.FromAtoms(curAtoms))

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func parseLine(s string) []types.Atom {
	regExp := regexp.MustCompile(`(\w+):(#?\w+)`)
	parsed := regExp.FindAllStringSubmatch(s, -1)
	atoms := []types.Atom{}
	for _, token := range parsed {
		atoms = append(atoms, types.Atom{
			Type:  token[1],
			Value: token[2],
		})
	}
	return atoms
}
