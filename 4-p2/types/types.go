package types

import (
	"regexp"
	"strconv"
)

type Atom struct {
	Type  string
	Value string
}

type Molecule struct {
	BirthYear      string
	IssueYear      string
	ExpirationYear string
	Height         string
	HairColor      string
	EyeColor       string
	PassportID     string
	CountryID      string
}

type Result struct {
	Res int
	Err error
}

func (m Molecule) FromAtoms(atoms []Atom) Molecule {
	for _, a := range atoms {
		switch a.Type {
		case "byr":
			m.BirthYear = a.Value
		case "iyr":
			m.IssueYear = a.Value
		case "eyr":
			m.ExpirationYear = a.Value
		case "hgt":
			m.Height = a.Value
		case "hcl":
			m.HairColor = a.Value
		case "ecl":
			m.EyeColor = a.Value
		case "pid":
			m.PassportID = a.Value
		case "cid":
			m.CountryID = a.Value
		}
	}
	return m
}

func (m Molecule) IsValid() bool {
	var rtr = true
	rtr = rtr && m.ValidateBirthYear()
	// fmt.Printf("\n\nValidateBirthYear: %v\n", rtr)
	rtr = rtr && m.ValidateIssueYear()
	// fmt.Printf("ValidateIssueYear: %v\n", rtr)
	rtr = rtr && m.ValidateExpirationYear()
	// fmt.Printf("ValidateExpirationYear: %v\n", rtr)
	rtr = rtr && m.ValidateHeight()
	// fmt.Printf("ValidateHeight: %v\n", rtr)
	rtr = rtr && m.ValidateHairColor()
	// fmt.Printf("ValidateHairColor: %v\n", rtr)
	rtr = rtr && m.ValidateEyeColor()
	// fmt.Printf("ValidateEyeColor: %v\n", rtr)
	rtr = rtr && m.ValidatePasswordId()
	// fmt.Printf("ValidatePasswordId: %v\n", rtr)
	return rtr
}

func (m Molecule) ValidateBirthYear() bool {
	return validateYear(m.BirthYear, 1920, 2002)

}

func (m Molecule) ValidateIssueYear() bool {
	return validateYear(m.IssueYear, 2010, 2020)
}

func (m Molecule) ValidateExpirationYear() bool {
	return validateYear(m.ExpirationYear, 2020, 2030)
}

func validateYear(s string, from int, to int) bool {
	regExp := regexp.MustCompile(`^\d{4}$`)
	parsed := regExp.FindAllString(s, -1)
	if parsed != nil {
		y, err := strconv.Atoi(parsed[0])
		if err != nil {
			return false
		}
		return y >= from && y <= to
	} else {
		return false
	}
}

func (m Molecule) ValidateHeight() bool {
	regExp := regexp.MustCompile(`^(\d+)(in|cm)$`)
	parsed := regExp.FindStringSubmatch(m.Height)
	if parsed != nil {
		y, err := strconv.Atoi(parsed[1])
		if err != nil {
			return false
		}
		u := parsed[2]
		if err != nil {
			return false
		}
		if u == "cm" {
			return y >= 150 && y <= 193
		} else {
			return y >= 59 && y <= 76
		}
	} else {
		return false
	}
}

func (m Molecule) ValidateHairColor() bool {
	regExp := regexp.MustCompile(`^#[0-9a-f]{6}$`)
	parsed := regExp.FindAllString(m.HairColor, -1)
	if parsed != nil {
		return true
	} else {
		return false
	}
}

func (m Molecule) ValidateEyeColor() bool {
	regExp := regexp.MustCompile(`^amb|blu|brn|gry|grn|hzl|oth$`)
	parsed := regExp.FindAllString(m.EyeColor, -1)
	if parsed != nil {
		return true
	} else {
		return false
	}
}

func (m Molecule) ValidatePasswordId() bool {
	regExp := regexp.MustCompile(`^\d{9}$`)
	parsed := regExp.FindAllString(m.PassportID, -1)
	if parsed != nil {
		return true
	} else {
		return false
	}
}
