package types

import (
	"fmt"
	"testing"
)

type tio struct {
	i Molecule
	o bool
}

func TestValidateBirthYear(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			BirthYear: "",
		},
		o: false,
	}, {
		i: Molecule{
			BirthYear: "1900",
		},
		o: false,
	}, {
		i: Molecule{
			BirthYear: "1920",
		},
		o: true,
	}, {
		i: Molecule{
			BirthYear: "1985",
		},
		o: true,
	}, {
		i: Molecule{
			BirthYear: "2002",
		},
		o: true,
	}, {
		i: Molecule{
			BirthYear: "2020",
		},
		o: false,
	}}
	for _, ti := range tis {
		var r bool = ti.i.ValidateBirthYear()
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

func TestValidateExpirationYear(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			ExpirationYear: "",
		},
		o: false,
	}, {
		i: Molecule{
			ExpirationYear: "2010",
		},
		o: false,
	}, {
		i: Molecule{
			ExpirationYear: "2020",
		},
		o: true,
	}, {
		i: Molecule{
			ExpirationYear: "2025",
		},
		o: true,
	}, {
		i: Molecule{
			ExpirationYear: "2030",
		},
		o: true,
	}, {
		i: Molecule{
			ExpirationYear: "2035",
		},
		o: false,
	}}
	for _, ti := range tis {
		var r bool = ti.i.ValidateExpirationYear()
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

func TestValidateIssueYear(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			IssueYear: "",
		},
		o: false,
	}, {
		i: Molecule{
			IssueYear: "2000",
		},
		o: false,
	}, {
		i: Molecule{
			IssueYear: "2010",
		},
		o: true,
	}, {
		i: Molecule{
			IssueYear: "2015",
		},
		o: true,
	}, {
		i: Molecule{
			IssueYear: "2020",
		},
		o: true,
	}, {
		i: Molecule{
			IssueYear: "2025",
		},
		o: false,
	}}
	for _, ti := range tis {
		var r bool = ti.i.ValidateIssueYear()
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

func TestValidateHeight(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			Height: "",
		},
		o: false,
	}, {
		i: Molecule{
			Height: "190",
		},
		o: false,
	}, {
		i: Molecule{
			Height: "cm",
		},
		o: false,
	}, {
		i: Molecule{
			Height: "in",
		},
		o: false,
	}, {
		i: Molecule{
			Height: "50in",
		},
		o: false,
	}, {
		i: Molecule{
			Height: "59in",
		},
		o: true,
	}, {
		i: Molecule{
			Height: "65in",
		},
		o: true,
	}, {
		i: Molecule{
			Height: "76in",
		},
		o: true,
	}, {
		i: Molecule{
			Height: "80in",
		},
		o: false,
	}, {
		i: Molecule{
			Height: "140cm",
		},
		o: false,
	}, {
		i: Molecule{
			Height: "150cm",
		},
		o: true,
	}, {
		i: Molecule{
			Height: "160cm",
		},
		o: true,
	}, {
		i: Molecule{
			Height: "193cm",
		},
		o: true,
	}, {
		i: Molecule{
			Height: "200cm",
		},
		o: false,
	}}
	for _, ti := range tis {
		var r bool = ti.i.ValidateHeight()
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

func TestValidateHairColor(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			HairColor: "",
		},
		o: false,
	}, {
		i: Molecule{
			HairColor: "#3032",
		},
		o: false,
	}, {
		i: Molecule{
			HairColor: "4039403#039203",
		},
		o: false,
	}, {
		i: Molecule{
			HairColor: "#303abz",
		},
		o: false,
	}, {
		i: Molecule{
			HairColor: "#438043",
		},
		o: true,
	}, {
		i: Molecule{
			HairColor: "#303abc",
		},
		o: true,
	}, {
		i: Molecule{
			HairColor: "#3a2ab4",
		},
		o: true,
	}}
	for _, ti := range tis {
		var r bool = ti.i.ValidateHairColor()
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

func TestValidateEyeColor(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			EyeColor: "",
		},
		o: false,
	}, {
		i: Molecule{
			EyeColor: "wat",
		},
		o: false,
	}, {
		i: Molecule{
			EyeColor: "dezezezrez",
		},
		o: false,
	}, {
		i: Molecule{
			EyeColor: "amb",
		},
		o: true,
	}, {
		i: Molecule{
			EyeColor: "blu",
		},
		o: true,
	}, {
		i: Molecule{
			EyeColor: "brn",
		},
		o: true,
	}, {
		i: Molecule{
			EyeColor: "gry",
		},
		o: true,
	}, {
		i: Molecule{
			EyeColor: "grn",
		},
		o: true,
	}, {
		i: Molecule{
			EyeColor: "hzl",
		},
		o: true,
	}, {
		i: Molecule{
			EyeColor: "oth",
		},
		o: true,
	},
	}
	for _, ti := range tis {
		var r bool = ti.i.ValidateEyeColor()
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

func TestValidatePasswordId(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			PassportID: "",
		},
		o: false,
	}, {
		i: Molecule{
			PassportID: "0123456789",
		},
		o: false,
	}, {
		i: Molecule{
			PassportID: "12345",
		},
		o: false,
	}, {
		i: Molecule{
			PassportID: "012345678",
		},
		o: true,
	}}
	for _, ti := range tis {
		var r bool = ti.i.ValidatePasswordId()
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

func TestPassport(t *testing.T) {
	var tis []tio = []tio{{
		i: Molecule{
			ExpirationYear: "1972",
			CountryID:      "100",
			HairColor:      "#18171d",
			EyeColor:       "amb",
			Height:         "170",
			PassportID:     "186cm",
			IssueYear:      "2018",
			BirthYear:      "1926",
		},
		o: false,
	}, {
		i: Molecule{
			IssueYear:      "2019",
			HairColor:      "#602927",
			ExpirationYear: "1967",
			Height:         "170cm",
			EyeColor:       "grn",
			PassportID:     "012533040",
			BirthYear:      "1946",
		},
		o: false,
	}, {
		i: Molecule{
			HairColor:      "dab227",
			IssueYear:      "2012",
			EyeColor:       "brn",
			Height:         "182cm",
			PassportID:     "021572410",
			ExpirationYear: "2020",
			BirthYear:      "1992",
			CountryID:      "277",
		},
		o: false,
	}, {
		i: Molecule{
			Height:         "59cm",
			EyeColor:       "zzz",
			ExpirationYear: "2038",
			HairColor:      "74454a",
			IssueYear:      "2023",
			PassportID:     "3556412378",
			BirthYear:      "2007",
		},
		o: false,
	}, {
		i: Molecule{
			PassportID:     "087499704",
			Height:         "74in",
			EyeColor:       "grn",
			IssueYear:      "2012",
			ExpirationYear: "2030",
			BirthYear:      "1980",
			HairColor:      "#623a2f",
		},
		o: true,
	}, {
		i: Molecule{
			ExpirationYear: "2029",
			EyeColor:       "blu",
			CountryID:      "129",
			BirthYear:      "1989",
			IssueYear:      "2014",
			PassportID:     "896056539",
			HairColor:      "#a97842",
			Height:         "165cm",
		},
		o: true,
	}, {
		i: Molecule{
			HairColor:      "#888785",
			Height:         "164cm",
			BirthYear:      "2001",
			IssueYear:      "2015",
			CountryID:      "88",
			PassportID:     "545766238",
			EyeColor:       "hzl",
			ExpirationYear: "2022",
		},
		o: true,
	}, {
		i: Molecule{
			IssueYear:      "2010",
			Height:         "158cm",
			HairColor:      "#b6652a",
			EyeColor:       "blu",
			BirthYear:      "1944",
			ExpirationYear: "2021",
			PassportID:     "093154719",
		},
		o: true,
	}}
	for _, ti := range tis {
		var r bool = ti.i.IsValid()
		if r != ti.o {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}
