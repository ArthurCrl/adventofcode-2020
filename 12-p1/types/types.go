package types
import (
	"fmt"
)


type Move string

const(
		North Move = "N"
		South Move = "S"
		East Move = "E"
		West Move = "W"
		Left Move = "L"
		Right Move = "R"
		Forward Move = "F"
)

type Atom struct {
	Action Move
	Nb int
}

func (a Atom) Equals(b Atom) bool {
	return fmt.Sprintf("%v", a) == fmt.Sprintf("%v", b)
}

type Molecule struct {
	Latitude int
	Longitude int
	Direction Move
}


func (m Molecule) Equals(n Molecule) bool {
	return fmt.Sprintf("%v", m) == fmt.Sprintf("%v", n)
}

func (m Molecule) FromAtoms(atoms *[]Atom) Molecule {
	return m
}
