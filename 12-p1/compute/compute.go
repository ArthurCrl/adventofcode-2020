package compute

import (
	"fmt"
	"../types"
)

func getMaps() (map[int]types.Move, map[types.Move]int) {
	mapOfAngleToDirection := map[int]types.Move{
		 90: types.North,
		 -270:types.North,
		 0: types.East,
		 360: types.East,
		 -90: types.South,
		 270: types.South,
		 180: types.West,
		 -180: types.West,
	}
	mapOfDirectionToAngle := map[types.Move]int{
		types.North:90,
		types.East: 0,
		types.South: -90,
		types.West: 180,
	}
	return mapOfAngleToDirection, mapOfDirectionToAngle
}
func ComputeAll(atoms []types.Atom) int {
	mol := types.Molecule{
		Longitude: 0,
		Latitude: 0,
		Direction: types.East,
	}
		fmt.Printf("%v\n", mol)
	for _, a := range atoms {
		if a.Action == types.Forward {
			ComputeMove(mol.Direction,a.Nb, &mol)
		} else {
			if a.Action == types.Left {
				mol.Direction = ComputeNewDirection(mol.Direction, a.Nb)
			} else if a.Action == types.Right {
				mol.Direction = ComputeNewDirection(mol.Direction, -1*a.Nb)
			} else {
				ComputeMove(a.Action, a.Nb, &mol)
			}
		}
		fmt.Printf("%v%v: %v\n", a.Action, a.Nb, mol) 
	}
	return Abs(mol.Longitude)+Abs(mol.Latitude)
}

func ComputeNewDirection(d types.Move, r int) types.Move {
	mapOfAngleToDirection, mapOfDirectionToAngle:= getMaps()
	curVal := mapOfDirectionToAngle[d]
	curVal = (curVal+r) % 360
	// fmt.Printf("d:%v, vd:%v, ag:%v, cv:%v, nd: %v\n",d,mapOfDirectionToAngle[d],r,curVal, mapOfAngleToDirection[curVal])
	return mapOfAngleToDirection[curVal]

}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func ComputeMove(m types.Move, nb int,  mol  *types.Molecule) {
	if m == types.North {
		mol.Longitude = mol.Longitude + nb
	}
	if m == types.South {
		mol.Longitude = mol.Longitude - nb
	}
	if m == types.East {
		mol.Latitude = mol.Latitude + nb
	}
	if m == types.West {
		mol.Latitude = mol.Latitude - nb
	}
}

