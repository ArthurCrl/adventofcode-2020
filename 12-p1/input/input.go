package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"../types"
	"regexp"
	"strconv"
)

func ParseAll(path string) []types.Atom {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Atom = []types.Atom{}
	for scanner.Scan() {
		atom := ParseLine(scanner.Text())
		rtrn = append(rtrn, atom)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}


func ParseLine(s string) types.Atom {
	regExp := regexp.MustCompile(`^([NESWLRF])(\d+)$`)
	parsed := regExp.FindStringSubmatch(s)
		
	if parsed != nil {
		nb := 0
		i, err := strconv.Atoi(parsed[2])
		if err == nil {
			nb = i
		}
		return types.Atom{
				Action: types.Move(parsed[1]),
				Nb: nb,
			}
	}
	
	return types.Atom{
		Action: types.Forward,
		Nb: 0,
	}
}
