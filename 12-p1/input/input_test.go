package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o  []types.Atom
}

func TestParseAll(t *testing.T) {
	var tis []tio = []tio{{
		i: "./test.txt",
		o: []types.Atom{
			types.Atom {
				Action: types.Forward,
				Nb: 10,
			},
			types.Atom {
				Action: types.North,
				Nb: 3,
			},
			types.Atom {
				Action: types.Forward,
				Nb: 7,
			},
			types.Atom {
				Action: types.Right,
				Nb: 90,
			},
			types.Atom {
				Action: types.Forward,
				Nb: 11,
			},
	}},
}
	for _, ti := range tis {
		var r []types.Atom = ParseAll(ti.i)
		if fmt.Sprintf("%v", r) != fmt.Sprintf("%v", ti.o) {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

