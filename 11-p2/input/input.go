package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"../types"
)

func ParseAll(path string) types.Atom {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn types.Atom = types.Atom{
		Iteration: 0,
		AllSpace: [][]types.Space{},
	}
	for scanner.Scan() {
		line := ParseLine(scanner.Text())
		rtrn.AllSpace = append(rtrn.AllSpace, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func ParseLine(s string) []types.Space {
	line := []types.Space{}
	for _,c := range s {
			line = append(line, types.Space(c))
	}
	return line
}
