package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o  types.Atom
}

func TestParseLine(t *testing.T) {
	var tis []tio = []tio{{
		i: "./test.txt",
		o: types.Atom{
			AllSpace: [][]types.Space{{
				types.Empty,
				types.Floor,
				types.Occ,
			},{
				types.Floor,
				types.Floor,
				types.Occ,
			}},
			Iteration: 0,
	}},
}
	for _, ti := range tis {
		var r types.Atom = ParseAll(ti.i)
		if !r.Equals(ti.o) {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}

