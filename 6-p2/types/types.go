package types
import (
	"fmt"
)
type Atom struct {
	Answers string
}

func (a Atom) Equals(b Atom) bool {
	return fmt.Sprintf("%v", a) == fmt.Sprintf("%v", b)
}

type Molecule struct {
	Atoms []Atom
	AnswersByChar map[rune]int 
	NbAnswers int
}


func (m Molecule) Equals(n Molecule) bool {
	return fmt.Sprintf("%v", m) == fmt.Sprintf("%v", n)
}

func (m Molecule) FromAtoms(atoms *[]Atom) Molecule {
	m.Atoms = *atoms
	return m
}

func (m Molecule) ComputeAnswers() Molecule {
	m.AnswersByChar = make(map[rune]int)
	m.NbAnswers = 0
	for _, a := range m.Atoms {
		for _, c := range a.Answers {
			m.AnswersByChar[c] = m.AnswersByChar[c] +1
		}
	}
	nbAtoms := len(m.Atoms)
	for _,v := range m.AnswersByChar {
		if v == nbAtoms {
			m.NbAnswers = m.NbAnswers +1
		}
	}
	return m
} 
