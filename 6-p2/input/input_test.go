package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o []types.Molecule
}

func TestParseAll(t *testing.T) {
	var tis []tio = []tio{{
		i: "./test.txt",
		o: []types.Molecule{{
			Atoms: []types.Atom{{
				Answers: "abc",
			},
			},
		}, {
			Atoms: []types.Atom{{
				Answers: "a",
			}, {
				Answers: "b",
			}, {
				Answers: "c",
			},
			},
		}, {
			Atoms: []types.Atom{{
				Answers: "ab",
			}, {
				Answers: "ac",
			},
			},
		}, {
			Atoms: []types.Atom{{
				Answers: "a",
			}, {
				Answers: "a",
			}, {
				Answers: "a",
			}, {
				Answers: "a",
			},
			},
		}, {
			Atoms: []types.Atom{{
				Answers: "b",
			},
			},
		},
		},
	},
	}
	for _, ti := range tis {
		var rs []types.Molecule = ParseAll(ti.i)
		t.Log("With input " + ti.i + " received " + fmt.Sprintf("%+v", rs))
		for i, r := range rs {
			var e types.Molecule
			if i < len(ti.o) {
				e = ti.o[i]
			}
			if !r.Equals(e) {
				t.Log("Result should be " + fmt.Sprintf("%+v", e) + ", but got " + fmt.Sprintf("%+v", r))
				t.Fail()
			}
		}
	}
}
