package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"../types"
)

func ParseAll(path string) []types.Molecule {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Molecule = []types.Molecule{}
	var curAtoms []types.Atom = []types.Atom{}
	for scanner.Scan() {
		atom := ParseLine(scanner.Text())
		if atom != nil{
			curAtoms = append(curAtoms, *atom)
		} else {
			rtrn = append(rtrn, types.Molecule{}.FromAtoms(&curAtoms))
			curAtoms = []types.Atom{}
		}
	}
	rtrn = append(rtrn, types.Molecule{}.FromAtoms(&curAtoms))

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func ParseLine(s string) *types.Atom {
	if s == "" {
		return nil
	}
	atom := types.Atom{
		Answers: s,
	}
	return &atom
}
