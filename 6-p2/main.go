package main

import (
	"./input"
	"./compute"
	"fmt"
)
func main() {
	var mols = input.ParseAll("./input/input.txt")
	var res = compute.ComputeAll(mols)
	fmt.Println(res)
}
