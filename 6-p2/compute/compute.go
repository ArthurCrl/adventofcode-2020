package compute

import (
	"../types"
)

func ComputeAll(mols []types.Molecule) int {
	cpt := 0
	for _, m := range mols {
		updatedMol := m.ComputeAnswers()
		cpt += updatedMol.NbAnswers
	}
	return cpt
}
