package compute

import (
	//  "fmt"
	"../types"
)

func ComputeAll(atoms []types.Atom, total int) int {
	cpt:= -1
	nbs := []int{}
	for _, a := range atoms {
		nbs = append(nbs,a.Nb)
	}
	index := FindTotal(nbs, total)
	cpt = FindSum(index, nbs, total)
	return cpt
}

func FindSum(indexFrom int, nbs []int, total int) int {
	slc := make([]int, len(nbs))
	copy(slc,  nbs)
	// fmt.Printf("nbs:%v\n",nbs)
	slc = slc[0 : indexFrom]
  for i:= 0 ; i< len(slc); i++ {
		cpt := 0
		streak := []int{}
		for j:= i; j< len(slc); j++ {
			cpt = cpt + slc[j]
			streak = append(streak, slc[j])
			if cpt == total {
				min, max := FindMinMax(streak)
				// fmt.Printf("i:%v j:%v \n",i, j)
				// fmt.Printf("min:%v max:%v \n",min, max)
				return min+max
			}
		}
	}
	return -1
}

func FindTotal(nbs []int, total int) int {
	for i, nb := range nbs {
		if nb == total {
			return i
		}
	}
	return -1
}

func FindMinMax(nbs []int) (int, int) {
	min := -1
	max := 0

	// fmt.Printf("nbs:%v\n",nbs)
	for _, v := range nbs {
		// fmt.Printf("min:%v max:%v \n",min, max)
		if v < min || min == -1 {
			min = v
		}
		if v > max {
			max = v
		}
	}
	return min, max
}
