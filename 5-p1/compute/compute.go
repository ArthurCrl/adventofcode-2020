package compute

import (
  "fmt"
	"../types"
)

func ComputeAll(atoms []types.Atom) int {
	cpt := 0
	for _, a := range atoms {
		computedAtom := ComputeAtom(a)
		if computedAtom.SeatId > cpt {
			cpt = computedAtom.SeatId
		}
	}
	return cpt
}

func ComputeAtom(a types.Atom) types.Atom {

	a.Row = computePathResult(a.RowPath, 0, 127)
	fmt.Printf("Computing row path %v\n", a.Row)
	a.Col = computePathResult(a.ColPath, 0, 7)
	fmt.Printf("Computing col path %v\n", a.Col)

	a.SeatId = a.Row * 8 + a.Col
	fmt.Printf("Computing seat id %v\n", a.SeatId)
	return a
}

func computePathResult(path []types.HalfPath, startingLowerBound int, startingUpperBound int) int {
	if len(path) == 0 {
		return -1
	}
	var lowerBound int = startingLowerBound
	var upperBound int = startingUpperBound
	for i:= 0; i<len(path)-1; i++ {
		v := path[i]
		if v == types.LowerHalf {
			upperBound = (upperBound - lowerBound)/ 2 + lowerBound
		} else {
			lowerBound =  (upperBound - lowerBound)/ 2 + lowerBound +1 
		}
	}
	if path[len(path)-1] == types.LowerHalf {
		return lowerBound
	} else {
	   return upperBound
	}
}
