package input

import (
	"fmt"
	"testing"

	"../types"
)

type tio struct {
	i string
	o types.Atom
}

func TestParseLine(t *testing.T) {
	var tis []tio = []tio{{
		i: "FBFBBFFRLR",
		o: types.Atom{
			RowPath: []types.HalfPath{
				types.LowerHalf,
				types.UpperHalf,
				types.LowerHalf,
				types.UpperHalf,
				types.UpperHalf,
				types.LowerHalf,
				types.LowerHalf,
			},
			ColPath: []types.HalfPath{
				types.UpperHalf,
				types.LowerHalf,
				types.UpperHalf,
			},
		},
	}}
	for _, ti := range tis {
		var r types.Atom = ParseLine(ti.i)
		if !r.Equals(ti.o) {
			t.Log("With input " + fmt.Sprintf("%+v", ti.i) + " result should be " + fmt.Sprintf("%+v", ti.o) + ", but got " + fmt.Sprintf("%+v", r))
			t.Fail()
		}
	}
}
