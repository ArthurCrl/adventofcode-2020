package input

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"../types"
)

func ParseAll(path string) []types.Atom {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Reading input file at " + absPath)
	file, err := os.Open(absPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var rtrn []types.Atom = []types.Atom{}
	for scanner.Scan() {
		atom := ParseLine(scanner.Text())
		rtrn = append(rtrn, atom)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return rtrn
}

func ParseLine(s string) types.Atom {
	atom := types.Atom{
		RowPath: []types.HalfPath{},
		ColPath: []types.HalfPath{},
	}
	for i, v := range s {
		var halfPath types.HalfPath
		if i < 7 {
			if v == 'F' {
				halfPath = types.LowerHalf 
			} else {
        halfPath = types.UpperHalf
			}  
			atom.RowPath = append(atom.RowPath, halfPath)
		} else {
			if v == 'L' {
				halfPath = types.LowerHalf 
			} else {
        halfPath = types.UpperHalf
			}  
			atom.ColPath = append(atom.ColPath, halfPath)
		}
	}
	return atom
}
