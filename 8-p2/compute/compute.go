package compute

import (
	// "fmt"

	"../types"
)

func ComputeAll(atoms []types.Atom) int {
	for j:= 0; j<len(atoms); j++ {
		fixedAtoms := ChangeAtomAtI(atoms, j)
		// fmt.Printf("Iteration %v: %v\n", j, atoms)
		cpt, broken := ComputeProgram(fixedAtoms)
		if !broken {
			return cpt
		}
	}
	return -1
}

func ComputeProgram(atoms []types.Atom) (int, bool) {
	cpt := 0
  broken := false
	mapOfVisitedIndexes := map[int]bool{}
	for i:=0; i<len(atoms);i++ {
		// fmt.Printf("i: %v\n", i)
		if 	mapOfVisitedIndexes[i] {
			broken = true
			break
		}
		mapOfVisitedIndexes[i]= true
		atom := atoms[i]
		if atom.Kind == types.Accumulate {
			cpt = cpt+ atom.Nb
		}
		if atom.Kind == types.Jump {
			i = i+atom.Nb-1
		}
	}
	// fmt.Printf("cpt: %v, broken: %v\n", cpt, broken)

	return cpt, broken
}

func ChangeAtomAtI(atoms []types.Atom, i int) []types.Atom {
	newAtoms := []types.Atom{}
	for j := 0; j<len(atoms); j++ {
		if j == i && atoms[j].Kind == types.Jump {
			newAtoms =  append(newAtoms, types.Atom{
				Kind: types.Nothing,
				Nb: 0,
			})
		} else {
			newAtoms =  append(newAtoms, atoms[j])
		}
	}
	return newAtoms
}
